package fi.themadhat.madracer.track;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class PiecePosition
{
    private int pieceIndex;
    private double inPieceDistance;
    private LanePosition lane;
    private int lap;

    public PiecePosition(int index)
    {
        this.pieceIndex = index;
        this.inPieceDistance = 0;
        this.lane = new LanePosition();
        this.lap = 0;
    }

    public PiecePosition()
    {
        this.pieceIndex = 0;
        this.inPieceDistance = 0;
        this.lane = new LanePosition();
        this.lap = 0;
    }

    public int getPieceIndex()
    {
        return pieceIndex;
    }

    public double getInPieceDistance()
    {
        return inPieceDistance;
    }

    public LanePosition getLane()
    {
        return lane;
    }

    public void setInPieceDistance(int inPieceDistance)
    {
        this.inPieceDistance = inPieceDistance;
    }

    public void setPieceIndex(int pieceIndex)
    {
        this.pieceIndex = pieceIndex;
    }

    public boolean switchingLanes()
    {
        return lane.getEndLaneIndex() != lane.getStartLaneIndex();
    }

    public void setInPieceDistance(double inPieceDistance)
    {
        this.inPieceDistance = inPieceDistance;
    }

    public void setLane(LanePosition lane)
    {
        this.lane = lane;
    }

    public int getLap()
    {
        return lap;
    }

    public void setLap(int lap)
    {
        this.lap = lap;
    }
}
