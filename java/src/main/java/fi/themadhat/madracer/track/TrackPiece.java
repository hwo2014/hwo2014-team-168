package fi.themadhat.madracer.track;

import com.google.gson.annotations.SerializedName;
import fi.themadhat.madracer.ai.MadRacer;
import fi.themadhat.madracer.ai.SpeedCalculator;
import fi.themadhat.madracer.race.CarPosition;
import fi.themadhat.madracer.race.Race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class TrackPiece
{
    private int radius;
    private double length;
    private double angle;
    @SerializedName("switch")
    private boolean laneSwitch;

    private int indexInTrack;

    private int combinedTrackCount;
    private TrackPiece nextPiece;

    public TrackPiece()
    {
        combinedTrackCount = 0;
    }

    public TrackPiece(double length, int radius, double angle)
    {
        this.radius = radius;
        this.angle = angle;
        this.length = length;
        combinedTrackCount = 0;
    }

    public int getIndexInTrack()
    {
        return indexInTrack;
    }

    public void setIndexInTrack(int indexInTrack)
    {
        this.indexInTrack = indexInTrack;
    }

    public boolean isLaneSwitch()
    {
        return laneSwitch;
    }

    public void setLaneSwitch(boolean laneSwitch)
    {
        this.laneSwitch = laneSwitch;
    }

    public int getRadius()
    {

        return radius;
    }

    public int getRadius(TrackLane lane)
    {
        if (angle < 0) {
            return radius + lane.getDistanceFromCenter();
        }
        if (angle > 0) {
            return radius - lane.getDistanceFromCenter();
        }
        return radius;
    }

    /**
     * Length of the track piece. If curved, calculated the length
     * from radiusa and angle.
     * @return
     */
    public double getLength()
    {
        if (radius != 0) {
            return Math.abs(2 * Math.PI * radius / 360 * angle);
        }
        return length;
    }

    /**
     * Length of the track piece. If curved, calculated the length
     * from radius, angle and lane position
     * @param lane
     * @return
     */
    public double getLength(TrackLane lane)
    {
        if (radius != 0) {
            return Math.abs(2 * Math.PI * getRadius(lane) / 360 * angle);
        }
        return length;
    }

    public void setRadius(int radius)
    {
        this.radius = radius;
    }

    public void setLength(double length)
    {
        this.length = length;
    }

    public double getAngle()
    {
        return angle;
    }

    public void setAngle(double angle)
    {
        this.angle = angle;
    }

    public boolean isStraight()
    {
        return radius == 0;
    }

    public double getAbsoluteAngle()
    {
        return Math.abs(angle);
    }

    public void addPiece(TrackPiece piece)
    {
        this.setAngle(Math.abs(this.getAngle()) + Math.abs(piece.getAngle()));
        if (this.getRadius() > 0) {
            // Was math.min
            //          this.setRadius(this.getRadius() + piece.getRadius());
        }
        else {
            this.setRadius(piece.getRadius());
        }
        this.combinedTrackCount++;
    }

    public boolean isOppositeRadius(TrackPiece sumPiece)
    {
        return ((this.getAngle() < 0 && sumPiece.getAngle() > 0)
                || (this.getAngle() > 0 && sumPiece.getAngle() < 0));
    }

    public double getCurveDifficulty()
    {
        double difficulty = Math.abs(angle) / 360;
        if (radius <= 100) {
            difficulty += difficulty * (100 - radius) / 100;
        }

        if (difficulty > 1) {
            return 1;
        }
        if (difficulty < 0) {
            return 0;
        }
        return difficulty;

    }

    public String toString()
    {
        return "Track piece, length " + length + ", radius " + radius + ", angle " + angle;
    }

    public void addAngle(double angle)
    {
        this.angle += angle;
    }

    public void addRadius(int radius)
    {
        this.radius += Math.abs(radius);
        this.combinedTrackCount++;
    }

    public void setNextPiece(TrackPiece nextPiece)
    {
        this.nextPiece = nextPiece;
    }

    /**
     * Calculated the maximum speed allowed for this piece.
     * @param race
     * @param myPosition
     * @return
     */
    public double calculateMaxSpeed(Race race, CarPosition myPosition, SpeedCalculator calculator, double distance)
    {
        if (indexInTrack == race.getTrack().getToughestCurveIndex())
        {
            if (getRadius() == 0)
            {
                return 999;
            }
            else
            {
                return Math.sqrt(getRadius(TrackCalculator.getLane(race, myPosition)) * calculator.getMaximumForceCurve());
            }
        }

        double nextSpeed = nextPiece.calculateMaxSpeed(race, myPosition, calculator, 0);
        double currentMaxSpeed = 0;
        // Straight piece
        if (getRadius() == 0)
        {
            currentMaxSpeed = 999;
        }
        else
        {
            currentMaxSpeed = Math.sqrt(getRadius(TrackCalculator.getLane(race, myPosition)) * calculator.getMaximumForceCurve());
        }
        if (nextSpeed > currentMaxSpeed)
        {
            return currentMaxSpeed;
        }
        double brakingDistance = 0;
        if (myPosition.getPieceIndex() != indexInTrack)
        {
            if (distance > 0)
            {
                TrackPiece myPiece = race.getPiece(myPosition.getPieceIndex());
                brakingDistance =  distance - myPiece.getLength() + myPosition.getPiecePosition().getInPieceDistance();
            }
            else
            {
                brakingDistance = getLength(TrackCalculator.getLane(race, myPosition));
            }
        }
        else
        {
            if (myPosition.getPiecePosition().getInPieceDistance() + distance >= race.getPiece(myPosition.getPieceIndex()).getLength(TrackCalculator.getLane(race, myPosition)))
            {
                return nextSpeed;
            }
            brakingDistance = getLength(TrackCalculator.getLane(race, myPosition)) - distance - myPosition.getPiecePosition().getInPieceDistance();
        }

        return Math.sqrt(Math.pow(nextSpeed, 2) + 2 * brakingDistance * calculator.getDragConstant());
    }
}
