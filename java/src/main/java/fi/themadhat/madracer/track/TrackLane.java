package fi.themadhat.madracer.track;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class TrackLane
{
    private int distanceFromCenter;
    private int index;

    public int getDistanceFromCenter()
    {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(int distanceFromCenter)
    {
        this.distanceFromCenter = distanceFromCenter;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}
