package fi.themadhat.madracer.track;

import fi.themadhat.madracer.ai.SpeedCalculator;
import fi.themadhat.madracer.race.CarPosition;
import fi.themadhat.madracer.race.Race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class TrackCalculator
{
    public static TrackPiece getNextCurve(Race race, CarPosition myPosition, int inclusion)
    {
        TrackPiece sumPiece = new TrackPiece();
        boolean indexDefined = false;
        if (inclusion == INCLUDE_CURRENT && !race.getPiece(myPosition.getPieceIndex()).isStraight()) {
            sumPiece.setIndexInTrack(myPosition.getPieceIndex());
            sumPiece.addPiece(race.getPiece(myPosition.getPieceIndex()));
            indexDefined = true;
            // Ei summata
            return race.getPiece(myPosition.getPieceIndex());
        }
        int counter = 1;
        // Ignore this curve
        if (inclusion == IGNORE_CURRENT && !race.getPiece(myPosition.getPieceIndex()).isStraight())
        {
            TrackPiece piece = race.getPiece(myPosition.getPieceIndex() + counter);
            while (!piece.isStraight())
            {
                counter++;
                piece = race.getPiece(myPosition.getPieceIndex() + counter);
            }
        }
        do {
            TrackPiece piece = race.getPiece(myPosition.getPieceIndex() + counter);
            if (piece.isStraight()) // || piece.isOppositeRadius(sumPiece))
            {
/*                if (sumPiece.getRadius() > 0 && piece.getLength() > 50) {
                    return sumPiece;
                }
                else if (sumPiece.getRadius() > 0) {
//                    System.out.println("Short straight piece " + piece.getLength());
                }
  */          }
            else {
                sumPiece.addPiece(piece);
                if (!indexDefined) {
                    sumPiece.setIndexInTrack(piece.getIndexInTrack());
                    indexDefined = true;
                }
                // Ei summata
                return piece;
            }
            counter++;
        } while (counter < race.getPieceCount());
        if (sumPiece.getAngle() > 360) {
            sumPiece.setAngle(360);
        }
        return sumPiece;
    }


    public static double getDistanceFromStart(Race race, CarPosition position)
    {
        if (position == null || position.getPiecePosition() == null)
        {
            return 0;
        }
        double distance = position.getPiecePosition().getInPieceDistance();
        int counter = 0;
        while (counter < position.getPieceIndex()) {
            TrackPiece piece = race.getPiece(counter);
            distance += piece.getLength();
            counter++;
        }

        if (position.getPiecePosition().getLap() > 1) {
            distance += position.getPiecePosition().getLap() * race.getTrack().getTotalLength();
        }
        return distance;
    }

    public static double distanceTo(Race race, TrackPiece piece, CarPosition position, LanePosition lane)
    {
        int targetIndex = piece.getIndexInTrack();
        int currentIndex = position.getPieceIndex();
        if (targetIndex == currentIndex) {
            return 0;
        }

        int max = targetIndex - currentIndex; // -1 => do not include target piece
        if (targetIndex < currentIndex) {
            max = max + race.getPieceCount() - currentIndex - 1; // -1 => do not include target piece
        }

        double distance = race.getTrack().getPiece(currentIndex).getLength(getLane(race, position)) - position.getPiecePosition().getInPieceDistance();
        int counter = 1;
//        System.out.println("Target " + piece.getIndexInTrack() + " Distance " + distance + " counter " + counter + " max " + max);
        while (counter < max) {
            TrackPiece trackPiece = race.getPiece(currentIndex + counter);
            distance += trackPiece.getLength(getLane(race, position));
            counter++;
            //          System.out.println("Distance " + distance + " counter " + counter + " max " + max);
        }
//        System.out.println("Distance to next curve is " + distance + ", " + max + " pieces, indexes " + targetIndex + " / " + currentIndex);
        return distance;
    }

    public static TrackLane getLane(Race race, CarPosition position)
    {
        return race.getTrack().getLanes().get(position.getLaneIndex());
    }


    public static double straightLeft(Race race, CarPosition myPosition, int mode)
    {
        boolean straight = true;
        TrackPiece myPiece = race.getPiece(myPosition.getPieceIndex());
        double straightDistance = 0;
        if (!myPiece.isStraight()) {
            if (mode == TrackCalculator.IGNORE_END_OF_CURVE) {
                // More than 50 still left in the curve piece => do not accelerate
                if (myPiece.getLength() - myPosition.getPiecePosition().getInPieceDistance() > 25) {
                    return 0.0;
                }
            }
            else {
                return 0.0;
            }
        }
        else {
            straightDistance = race.getPiece(myPosition.getPieceIndex()).getLength() - myPosition.getPiecePosition().getInPieceDistance();
        }
        int counter = 1;
        do {
            TrackPiece piece = race.getPiece(myPosition.getPieceIndex() + counter);
            if (!piece.isStraight()) {
                straight = false;
            }
            else {
                straightDistance += piece.getLength();
            }
            counter++;
        } while (straight && counter < race.getPieceCount());
        return straightDistance;
    }


    public static double getDistanceLeftInPiece(Race race, PiecePosition piecePosition)
    {
        TrackPiece piece = race.getPiece(piecePosition.getPieceIndex());
        if (piece.isStraight()) {
            return piece.getLength() - piecePosition.getInPieceDistance();
        }
        else {
            return piece.getRadius() - piecePosition.getInPieceDistance();
        }
    }
    public static boolean switchBeforeCurve(Race race, CarPosition myPosition, TrackPiece nextCurve)
    {
        int index = myPosition.getPieceIndex();
//        System.out.println("My position" + index);
        int max = race.getPieceCount();
        //      System.out.println("Race piece count" + race.getPieceCount());
        if (nextCurve.getIndexInTrack() > index) {
            max = nextCurve.getIndexInTrack();
            //            System.out.println("Max is curve index " + max);
        }
        index++;
        while (index < max) {
            if (race.getTrack().getPiece(index).isLaneSwitch()) {
                return true;
            }
            index++;
        }
        if (max != nextCurve.getIndexInTrack()) {
            index = 0;
            while (index < nextCurve.getIndexInTrack()) {
                if (race.getTrack().getPiece(index).isLaneSwitch()) {
                    return true;
                }
                index++;
            }
        }
        return false;
    }

    public static double getMaxSpeed(Race race, CarPosition myPosition, SpeedCalculator calculator, double distance)
    {
        TrackPiece piece = race.getTrack().getPiece(myPosition.getPieceIndex());
        return piece.calculateMaxSpeed(race, myPosition, calculator, distance);
    }

    public static final int IGNORE_CURRENT = 1;
    public static final int INCLUDE_CURRENT = 0;
    public static final int IGNORE_END_OF_CURVE = 3;

}
