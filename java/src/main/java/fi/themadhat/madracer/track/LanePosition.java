package fi.themadhat.madracer.track;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class LanePosition
{
    private int startLaneIndex;
    private int endLaneIndex;

    public int getStartLaneIndex()
    {
        return startLaneIndex;
    }

    public int getEndLaneIndex()
    {
        return endLaneIndex;
    }
}
