package fi.themadhat.madracer.track;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class RaceTrack
{
    private String id;
    private String name;
    private List<TrackPiece> pieces;
    private List<TrackLane> lanes;
    private Point startingPoint;
    private int toughestCurveIndex;

    private double c1;
    private double c2;
    private double c3;

    public RaceTrack()
    {
        this.pieces = new ArrayList<TrackPiece>();
        this.lanes = new ArrayList<TrackLane>();
    }

    public String getName()
    {
        return name;
    }

    public List<TrackPiece> getPieces()
    {
        return pieces;
    }

    public TrackPiece getPiece(int i)
    {
        return pieces.get(i);
    }

    public List<TrackLane> getLanes()
    {
        return lanes;
    }

    public double getTotalLength()
    {
        double length = 0;
        for (TrackPiece trackPiece : pieces) {
            length += trackPiece.getLength();
        }

        return length;
    }

    public void addPiece(int index, TrackPiece trackPiece)
    {
        this.pieces.add(index, trackPiece);
        trackPiece.setIndexInTrack(index);
    }

    public void calculateMaxSpeeds()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setPieces(List<TrackPiece> pieces)
    {
        this.pieces = pieces;
    }

    public void setLanes(List<TrackLane> lanes)
    {
        this.lanes = lanes;
    }

    public Point getStartingPoint()
    {
        return startingPoint;
    }

    public void setStartingPoint(Point startingPoint)
    {
        this.startingPoint = startingPoint;
    }

    public int getToughestCurveIndex()
    {
        return toughestCurveIndex;
    }

    public void setToughestCurveIndex(int toughestCurveIndex)
    {
        this.toughestCurveIndex = toughestCurveIndex;
    }

    public double getC1()
    {
        return c1;
    }

    public void setC1(double c1)
    {
        this.c1 = c1;
    }

    public double getC2()
    {
        return c2;
    }

    public void setC2(double c2)
    {
        this.c2 = c2;
    }

    public double getC3()
    {
        return c3;
    }

    public void setC3(double c3)
    {
        this.c3 = c3;
    }
}