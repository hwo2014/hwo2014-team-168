package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class CarSpec
{
    public final String name;
    public final String color;
    private boolean angle;

    public CarSpec()
    {
        this.name = "";
        this.color = "";
    }

    public CarSpec(String name, String color)
    {
        this.name = name;
        this.color = color;
    }

    public String getColor()
    {
        return color;
    }

    public String getName()
    {
        return name;
    }

    public boolean equals(Object o)
    {
        if (o instanceof CarSpec) {
            return this.name.equals(((CarSpec) o).getName()) && this.color.equals(((CarSpec) o).getColor());
        }
        return false;
    }

    public int hashCode()
    {
        return (this.name + "|" + this.color).hashCode();
    }

    public boolean getAngle()
    {
        return angle;
    }
}
