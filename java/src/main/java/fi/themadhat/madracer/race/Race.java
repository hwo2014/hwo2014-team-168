package fi.themadhat.madracer.race;

import fi.themadhat.madracer.track.RaceTrack;
import fi.themadhat.madracer.track.TrackCalculator;
import fi.themadhat.madracer.track.TrackPiece;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class Race
{

    public final RaceTrack track;
    public final List<Car> cars;
    public final RaceSession raceSession;
    private long gameTicks;

    public Race(RaceTrack track, List<Car> cars, RaceSession raceSession)
    {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }

    private boolean raceOn;

    public Race(RaceTrack track)
    {
        this.track = track;
        this.raceSession = new RaceSession();
        this.cars = new ArrayList<Car>();
    }

    public void end()
    {
        raceOn = false;
    }

    public void start()
    {
        raceOn = true;
    }

    public void processPositions(Race race, List<CarPosition> message, double gameTick)
    {
        this.gameTicks = (long) gameTick;
        for (CarPosition carPosition : message) {
            for (Car car : cars) {
                if (car.getId().equals(carPosition.getId()))
                {
                    double distanceFromStart = car.getLatestDistance();
                    if (car.getPosition() != null && car.getPosition().getPieceIndex() != carPosition.getPieceIndex())
                    {
                        TrackPiece piece = race.getPiece(car.getPosition().getPieceIndex());
                        distanceFromStart = distanceFromStart - piece.getLength(TrackCalculator.getLane(race, car.getPosition()));
                    }

                    carPosition.setGameTick(gameTick);
                    car.setPosition(carPosition);
                    car.setCurrentSpeed( (car.getPosition().getPiecePosition().getInPieceDistance() - distanceFromStart) / (gameTick - car.getPreviousGameTick));
                    car.setLatestDistance(car.getPosition().getPiecePosition().getInPieceDistance());
                    car.setPreviousGameTick(gameTick);
                }
            }
        }
    }

    public boolean isOn()
    {
        return raceOn;
    }

    public RaceTrack getTrack()
    {
        return track;
    }

    public CarPosition locatePosition(CarSpec myCar)
    {
        for (Car car : cars) {
            if (car.getId().equals(myCar)) {
                return car.getPosition();
            }
        }
        return null;
    }

    public List<Car> getCars()
    {
        return cars;
    }

    public void setGameTicks(long gameTicks)
    {
        this.gameTicks = gameTicks;
    }

    public boolean isRaceOn()
    {
        return raceOn;
    }

    public void setRaceOn(boolean raceOn)
    {
        this.raceOn = raceOn;
    }

    public int getPieceCount()
    {
        return getTrack().getPieces().size();
    }

    public void calculateTracks()
    {
        int index = 0;
        for (TrackPiece trackPiece : track.getPieces()) {
            trackPiece.setIndexInTrack(index++);
        }
        List<TrackPiece> pieces = track.getPieces();
        int radius = 9999;
        for (int i = 0; i < pieces.size(); i++)
        {
            if (i == pieces.size() - 1)
            {
                pieces.get(i).setNextPiece(pieces.get(0));
            }
            else {
                pieces.get(i).setNextPiece(pieces.get(i+1));
            }
            if (pieces.get(i).getRadius() > 0
                    && pieces.get(i).getRadius() < radius)
            {
                radius = pieces.get(i).getRadius();
                track.setToughestCurveIndex(i);
            }
        }
    }

    public void setOn(boolean on)
    {
        this.raceOn = on;
    }

    public RaceSession getRaceSession()
    {
        return raceSession;
    }


    /**
     * Return a piece that matches the given index.
     *
     * @param pieceIndex
     * @return
     */
    public TrackPiece getPiece(int pieceIndex)
    {
        int index = pieceIndex;
        while (index >= getPieceCount()) {
            index -= getPieceCount();
        }
        return track.getPiece(index);
    }

    public long getGameTicks()
    {
        return gameTicks;
    }

}
