package fi.themadhat.madracer.race;

import fi.themadhat.madracer.track.PiecePosition;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class CarPosition
{
    private CarSpec id;
    private double angle;
    private PiecePosition piecePosition;
    private double gameTick;

    public CarSpec getId()
    {
        return id;
    }

    public double getAngle()
    {
        return angle;
    }


    public PiecePosition getPiecePosition()
    {
        return piecePosition;
    }

    public int getPieceIndex()
    {
        return piecePosition.getPieceIndex();
    }


    public void setId(CarSpec id)
    {
        this.id = id;
    }

    public void setPiecePosition(PiecePosition piecePosition)
    {
        this.piecePosition = piecePosition;
    }

    public void setAngle(double angle)
    {
        this.angle = angle;
    }
    public double getGameTick()
    {
        return gameTick;
    }

    public void setGameTick(double gameTick)
    {
        this.gameTick = gameTick;
    }

    public int getLaneIndex()
    {
        return piecePosition.getLane().getEndLaneIndex();
    }
}


