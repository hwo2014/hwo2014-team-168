package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class Car
{
    private CarSpec id;
    private Dimension dimensions;
    private CarPosition position;
    private double currentSpeed;
    private double latestDistance;
    public double getPreviousGameTick;
    private double previousGameTick;

    public CarSpec getId()
    {
        return id;
    }

    public CarPosition getPosition()
    {
        return position;
    }

    public void setPosition(CarPosition carPosition)
    {
        position = carPosition;
    }

    public void setId(CarSpec id)
    {
        this.id = id;
    }

    public Dimension getDimensions()
    {
        return dimensions;
    }

    public void setDimensions(Dimension dimensions)
    {
        this.dimensions = dimensions;
    }

    public void setCurrentSpeed(double speed)
    {
        this.currentSpeed = speed;
    }

    public double getCurrentSpeed()
    {
        return currentSpeed;
    }

    public double getAngle()
    {

        if (position != null)
            return position.getAngle();
        return 0;
    }

    public double getLatestDistance()
    {
        return latestDistance;
    }

    public void setLatestDistance(double latestDistance)
    {
        this.latestDistance = latestDistance;
    }

    public void setPreviousGameTick(double previousGameTick)
    {
        this.previousGameTick = previousGameTick;
    }

    public double getPreviousGameTick()
    {
        return previousGameTick;
    }

    private class Dimension
    {
        private double length;
        private double width;
        private double guideFlagPosition;
    }

}
