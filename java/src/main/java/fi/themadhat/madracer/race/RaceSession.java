package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class RaceSession
{
    // Qualifying
    private long durationMs;

    // Race
    private int laps;
    private float maxLapTimeMs;
    private boolean quickRace;

    public RaceSession()
    {
        this.durationMs = 0;
        this.laps = 0;
        this.maxLapTimeMs = 0;
        this.quickRace = false;
    }

    public int getLaps()
    {
        return laps;
    }

    public void setLaps(int laps)
    {
        this.laps = laps;
    }

    public float getMaxLapTimeMs()
    {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(float maxLapTimeMs)
    {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public boolean isQuickRace()
    {
        return quickRace;
    }

    public void setQuickRace(boolean quickRace)
    {
        this.quickRace = quickRace;
    }

    public long getDurationMs()
    {
        return durationMs;
    }

    public void setDurationMs(long durationMs)
    {
        this.durationMs = durationMs;
    }

    public boolean isQualifying()
    {
        return this.durationMs > 0 && this.laps == 0;
    }
}
