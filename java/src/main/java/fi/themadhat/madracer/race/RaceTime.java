package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class RaceTime
{
    private long millis;
    private int laps;
    private long ticks;

    public long getMillis()
    {
        return millis;
    }

    public void setMillis(long millis)
    {
        this.millis = millis;
    }

    public int getLaps()
    {
        return laps;
    }

    public void setLaps(int laps)
    {
        this.laps = laps;
    }

    public long getTicks()
    {
        return ticks;
    }

    public void setTicks(long ticks)
    {
        this.ticks = ticks;
    }
}
