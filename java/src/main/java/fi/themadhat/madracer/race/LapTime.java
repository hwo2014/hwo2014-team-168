package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class LapTime
{
    private long millis;
    private int lap;
    private long ticks;

    public long getMillis()
    {
        return millis;
    }

    public long getLap()
    {
        return lap;
    }

    public void setMillis(long millis)
    {
        this.millis = millis;
    }

    public void setLap(int lap)
    {
        this.lap = lap;
    }

    public long getTicks()
    {
        return ticks;
    }

    public void setTicks(long ticks)
    {
        this.ticks = ticks;
    }
}
