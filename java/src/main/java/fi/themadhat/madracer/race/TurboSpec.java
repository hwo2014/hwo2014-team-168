package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class TurboSpec
{
    private double turboDurationMilliseconds;
    private long turboDurationTicks;
    private double turboFactor;
    private long currentTicks;
    private long startTicks;

    public double getTurboFactor()
    {
        return turboFactor;
    }

    public void setTurboFactor(double turboFactor)
    {
        this.turboFactor = turboFactor;
    }

    public double getTurboDurationMilliseconds()
    {
        return turboDurationMilliseconds;
    }

    public void setTurboDurationMilliseconds(double turboDurationMilliseconds)
    {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    public long getTurboDurationTicks()
    {
        return turboDurationTicks;
    }

    public void setTurboDurationTicks(long turboDurationTicks)
    {
        this.turboDurationTicks = turboDurationTicks;
    }

    public boolean isOn()
    {
        return (currentTicks - startTicks) < turboDurationTicks;
    }

    public long getStartTicks()
    {
        return startTicks;
    }

    public void setStartTicks(long startTicks)
    {
        this.startTicks = startTicks;
    }

    public long getCurrentTicks()
    {
        return currentTicks;
    }

    public void setCurrentTicks(long currentTicks)
    {
        this.currentTicks = currentTicks;
    }
}
