package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class Disqualification
{
    private CarSpec car;
    private String reason;

    public CarSpec getCar()
    {
        return car;
    }

    public void setCar(CarSpec car)
    {
        this.car = car;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }
}
