package fi.themadhat.madracer.race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class Ranking
{
    private int overall;
    private int fastestLap;

    public int getOverall()
    {
        return overall;
    }

    public void setOverall(int overall)
    {
        this.overall = overall;
    }

    public int getFastestLap()
    {
        return fastestLap;
    }

    public void setFastestLap(int fastestLap)
    {
        this.fastestLap = fastestLap;
    }
}
