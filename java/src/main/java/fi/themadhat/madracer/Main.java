package fi.themadhat.madracer;

import com.google.gson.Gson;
import fi.themadhat.madracer.ai.MadRacer;
import fi.themadhat.madracer.messaging.*;
import fi.themadhat.madracer.race.Race;
import fi.themadhat.madracer.track.TrackPiece;

import java.io.*;
import java.net.Socket;

public class Main
{
    public static void main(String[] args)
            throws IOException
    {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String track = null;
        if (args.length > 4) {
            track = args[4];
        }
        int bots = 1;
        if (args.length > 5) {
            bots = Integer.parseInt(args[5]);
        }
        String password = null;
        if (args.length > 6) {
            password = args[6];
        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        //  System.out.println(bots + " bots in the race");
        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new BotData(botName, botKey), track, bots, password);
        socket.close();
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final BotData join,  String track, int bots, String password)
            throws IOException
    {
        this.writer = writer;
        String line;

        Race currentRace = null;
        MadRacer madracer = new MadRacer();

        if (track != null && track.length() > 0) {
            send(new JoinRaceMsg(join, track, password, bots));
        }
        else {
            send(new JoinMsg(join));
        }
        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.getMsgType().equals("carPositions")) {
                if (madracer.getRace().isOn()) {
                    CarPositionsMsg message = gson.fromJson(line, CarPositionsMsg.class);
                    madracer.processPositions(message.getData(), message.getGameTick());
                    send(madracer.createMessage(), msgFromServer.getGameTick());
                }
                else {
                    send(new Ping(), msgFromServer.getGameTick());
                }
            }
            else if (msgFromServer.getMsgType().equals("join")) {
                System.out.println("Joined");
            }
            else if (msgFromServer.getMsgType().equals("tournamentEnd")) {
                System.out.println("Tournament ended");
            }
            else if (msgFromServer.getMsgType().equals("yourCar")) {
                YourCarMsg message = gson.fromJson(line, YourCarMsg.class);
                madracer.setMyCarSpec(message.getData());
                madracer.setColor(message.getData().getColor());
                System.out.println("My car is " + madracer.getColor());
            }
            else if (msgFromServer.getMsgType().equals("turboAvailable")) {
                TurboAvailableMsg message = gson.fromJson(line, TurboAvailableMsg.class);
                madracer.setTurboAvailable(message.getData());
                System.out.println("Turbo available");
            }
            else if (msgFromServer.getMsgType().equals("gameInit")) {
                GameInitMsg message = gson.fromJson(line, GameInitMsg.class);
                currentRace = message.getData().getRace();
                currentRace.calculateTracks();
                madracer.joinRace(currentRace);
                madracer.setStrategy(MadRacer.ADJUSTING);
                System.out.println("Race init for track " + currentRace.getTrack().getName());
                for (TrackPiece piece : currentRace.getTrack().getPieces())
                {
                    System.out.println(piece.getIndexInTrack() + " = " + piece.getLength() + " r " + piece.getRadius() + " a " + piece.getAngle());
                }
            }
            else if (msgFromServer.getMsgType().equals("gameEnd")) {
                currentRace.end();
                System.out.println("Race end. Current curve multiplier is " + madracer.getCurrentCurveMultiplier() + ", max angle "
                        + madracer.getMaxAngle());
                madracer.leaveRace();
            }
            else if (msgFromServer.getMsgType().equals("gameStart")) {
                currentRace.start();
                System.out.println("Race start");
                send(madracer.createMessage(), msgFromServer.getGameTick());
            }
            else if (msgFromServer.getMsgType().equals("crash")) {
                CrashedMsg message = gson.fromJson(line, CrashedMsg.class);
                if (message.getData().equals(madracer.getMyCar().getId()))
                {
                    madracer.crashed();
                    System.out.println(message.getData().getName() + " crashed, last speed " + madracer.getOkSpeed());
                }
                else {
                    System.out.println(message.getData().getName() + " crashed");
                }
            }
            else if (msgFromServer.getMsgType().equals("lapFinished")) {
                LapFinishedMsg message = gson.fromJson(line, LapFinishedMsg.class);
                System.out.println(message.getData().getCar().getName() + " finished lap " + message.getData().getLapTime().getLap() + " with time " + message.getData().getLapTime().getMillis());
            }
            else if (msgFromServer.getMsgType().equals("spawn")) {
                SpawnMsg message = gson.fromJson(line, SpawnMsg.class);
                if (message.getData().equals(madracer.getMyCar().getId())) {
                    madracer.spawned();
                    System.out.println("Car spawned");
                }
            }
            else if (msgFromServer.getMsgType().equals("dnf"))
            {
                DnfMsg message = gson.fromJson(line, DnfMsg.class);
                System.out.println(message.getData().getCar().getName() + " disqualified, reason: " + message.getData().getReason());
            }
            else if (msgFromServer.getMsgType().equals("finish"))
            {
                FinishMsg message = gson.fromJson(line, FinishMsg.class);
                System.out.println(message.getData().getName() + " finished");
            }
            else {
                send(new Ping(), msgFromServer.getGameTick());
            }
        }
    }

    private void send(final Message msg)
    {
        writer.println(msg.toJson());
        writer.flush();
    }
    private void send(final Message msg, long gameTick)
    {
        msg.setGameTick(gameTick);
        writer.println(msg.toJson());
        writer.flush();
    }
}

