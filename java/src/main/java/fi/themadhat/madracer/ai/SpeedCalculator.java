package fi.themadhat.madracer.ai;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class SpeedCalculator
{
    private double dragConstant = 0.1;
    private double massOfCar = 2.4;
    private double maximumForceCurve = 0.55;

    private double startGameTick = 1;

    private double speedT1;
    private double speedT2;
    private double speedT3;
    private boolean maximumForceCurveInitialized = false;
    private boolean dragConstantInitialized = false;
    private boolean massOfCarInitialized = false;
    private double deacceleration = 0.1; //25;

    public double getMaximumForceCurve()
    {
        return maximumForceCurve;
    }

    public void setMaximumForceCurve(double maximumForceCurve)
    {
        this.maximumForceCurve = maximumForceCurve;
    }

    public double getMassOfCar()
    {
        return massOfCar;
    }

    public void setMassOfCar(double massOfCar)
    {
        this.massOfCar = massOfCar;
    }

    public double getDragConstant()
    {
        return dragConstant;
    }

    public void setDragConstant(double dragConstant)
    {
        this.dragConstant = dragConstant;
    }

    public void setSpeed(double velocity, long gameTick, double throttle)
    {
        System.out.println("Set speed " + velocity + " at " + gameTick + " for thr " + throttle);
        if (startGameTick == 1)
        {
            speedT1 = velocity;
            startGameTick++;
        }
        else if (startGameTick == 2)
        {
            speedT2 = velocity;
            startGameTick++;
        }
        else if (startGameTick == 3)
        {
            speedT3 = velocity;
            dragConstant = (speedT2 - (speedT3 - speedT2)) / Math.pow(speedT2, 2) * throttle;
            if (dragConstant < 0.1)
            {
                dragConstant = 0.1;
            }
            if (dragConstant > 0.99)
            {
                dragConstant = 0.99;
            }
            dragConstantInitialized = true;
            startGameTick++;
        }
        else if (startGameTick == 4)
        {
            double speedT4 = velocity;
            massOfCar = 1 / (Math.log((speedT4 - throttle / dragConstant) / (speedT2 - throttle / dragConstant)) / (-1 * dragConstant));
            massOfCarInitialized = true;
        }
    }

    public boolean isMaximumForceCurveInitialized()
    {
        return maximumForceCurveInitialized;
    }

    public void setMaximumForceCurveInitialized(boolean maximumForceCurveInitialized)
    {
        this.maximumForceCurveInitialized = maximumForceCurveInitialized;
    }

    public boolean isDragConstantInitialized()
    {
        return dragConstantInitialized;
    }

    public void setDragConstantInitialized(boolean dragConstantInitialized)
    {
        this.dragConstantInitialized = dragConstantInitialized;
    }

    public void setCrashSpeed(double velocity, double radius)
    {
        if (radius != 0 && velocity > 5) {
            maximumForceCurve = Math.floor(Math.pow(velocity - 0.1, 2) / radius * 500) / 500;
            maximumForceCurveInitialized = true;
     //       System.out.println("Max force " + maximumForceCurve);
        }
    }

    public double getMaximumSpeed(double radius)
    {
        return Math.sqrt(maximumForceCurve * radius);
    }

    public double getThrottle(double targetVelocity, double turboFactor)
    {
        return targetVelocity * dragConstant / turboFactor;
    }

    public double getDeacceleration()
    {
        return deacceleration;
    }

    public double getMaxVelocityGainedInTime(double reactionTime)
    {
        return (1 / dragConstant) * Math.exp(-1 * dragConstant * reactionTime / massOfCar) + 1/dragConstant;
    }

    public boolean isMassOfCarInitialized()
    {
        return massOfCarInitialized;
    }
}
