package fi.themadhat.madracer.ai;

import fi.themadhat.madracer.messaging.Message;
import fi.themadhat.madracer.messaging.SwitchLaneMsg;
import fi.themadhat.madracer.messaging.Throttle;
import fi.themadhat.madracer.messaging.TurboMsg;
import fi.themadhat.madracer.race.*;
import fi.themadhat.madracer.track.PiecePosition;
import fi.themadhat.madracer.track.TrackCalculator;
import fi.themadhat.madracer.track.TrackPiece;

import java.util.List;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class MadRacer
{
    private double currentFriction = 0.016;
    private  double currentCurveMultiplier = 3.2;
    private static final double MAX_ANGLE = 56.5;
    public static final double REACTION_TIME = 1;
    private Race race;
    private Car myCar;
    private String color;

    private double maxAngle = 0;
    private double maxAngleInPiece = 0;
    private double currentThrottle = 0;

    private int strategy;
    private int switchSent = 0;
    private double lastAngle;
    private int privateCounter = 0;
    private double maxSpeed = 0;
    private double currentPosition = 0;
    private TurboSpec turboAvailable;
    private double maxAnglePieceAngle = 0;
    private int maxAngleRadius;

    private TurboSpec turboInUse;
    private CompetitorWatcher watcher;

    private SpeedCalculator calculator;
    private double speedCorrection = 0;

    private double okSpeed = 0;
    private boolean switched = false;
    private int initialLane = -1;
    private int lastRadius = 0;

    public void processPositions(List<CarPosition> data, long gameTick)
    {
        okSpeed = myCar.getCurrentSpeed();
        double distanceFromStart = 0;
        double previousGameTick = 0;
        if (myCar.getPosition() != null) {
//            distanceFromStart = TrackCalculator.getDistanceFromStart(race, myCar.getPosition());
            distanceFromStart = myCar.getPosition().getPiecePosition().getInPieceDistance();
            previousGameTick = myCar.getPosition().getGameTick();
        }
        race.processPositions(race, data, gameTick);
        CarPosition newPosition = race.locatePosition(myCar.getId());
        // Piece completed
        if (myCar.getPosition() != null && myCar.getPosition().getPieceIndex() != newPosition.getPieceIndex())
        {
            initialLane = myCar.getPosition().getPiecePosition().getLane().getStartLaneIndex();
            TrackPiece piece = race.getPiece(myCar.getPosition().getPieceIndex());
            if (piece.getRadius() != 0)
            {
                lastRadius = piece.getRadius();
            }
            distanceFromStart = distanceFromStart - piece.getLength(TrackCalculator.getLane(race, myCar.getPosition()));

            // Lap changed, tune constants according to my car angle
            if (myCar.getPosition().getPiecePosition().getLap() < newPosition.getPiecePosition().getLap())
            {
                if (maxAngleInPiece > 56.5) {
                    currentCurveMultiplier -= 0.05;
                }
                else if (maxAngleInPiece < 15) {
                    currentCurveMultiplier += 0.3;
                }
                else if (maxAngleInPiece < 25) {
                    currentCurveMultiplier += 0.2;
                }
                else if (maxAngleInPiece < 35) {
                    currentCurveMultiplier += 0.15;
                }
                else if (maxAngleInPiece < 45) {
                    currentCurveMultiplier += 0.1;
                }
                else if (maxAngleInPiece < 55) {
                    currentCurveMultiplier += 0.05;
                }
                maxAngleInPiece = 0;
            }
        }
        else
        {
            if (maxAngleInPiece < Math.abs(myCar.getAngle()))
            {
                maxAngleInPiece = Math.abs(myCar.getAngle());
            }
        }
        myCar.setPosition(newPosition);
        lastAngle = myCar.getPosition().getAngle();
        if (myCar.getPosition().getPiecePosition().switchingLanes()) {
            switchSent = 0;
        }
        if (gameTick > 0)
        {
            myCar.setCurrentSpeed( (myCar.getPosition().getPiecePosition().getInPieceDistance() - distanceFromStart) / (gameTick - previousGameTick));
          //  System.out.println(myCar.getCurrentSpeed());
            if (!calculator.isDragConstantInitialized() || !calculator.isMassOfCarInitialized())
            {
                calculator.setSpeed(myCar.getCurrentSpeed(), gameTick, 1.0);
            }
        }
        if (Math.abs(lastAngle) > maxAngle)
        {
            maxAngle = lastAngle;
            maxAngleRadius = race.getPiece(newPosition.getPieceIndex()).getRadius();
            maxAnglePieceAngle = race.getPiece(newPosition.getPieceIndex()).getAngle();
        }
    }

    /**
     * Create a message (throttle, switch, turbo)
     * @return
     */
    public Message createMessage()
    {
        Message msg = null;
        if (race.getGameTicks() > 3) {
            if (watcher != null && watcher.actionIsWise()) {
                if (switchSent == 0 && watcher.switchIsWise()) {
                    System.out.println("Should change lanes");
                    msg = createSwitchMessage(watcher.getSwitchDirection());
                    watcher.actionTaken();
                }
                else if (isTurboAvailable() && watcher.isTurboWise()) {
                    msg = createTurboMessage();
                    System.out.println("Should use turbo");
                    watcher.actionTaken();
                }
                else if (watcher.isBrakeWise() && watcher.getWiseSpeed() > 0
                        && watcher.getWiseSpeed() < myCar.getCurrentSpeed())
                {
                    msg = createThrottleMessage(watcher.getWiseSpeed());
                    System.out.println("Should ease on the throttle");
                    watcher.actionTaken();
                }
            }
            else {
                msg = createSwitchMessage();
            }
        }
        if (msg == null) {
            msg = createThrottleMessage();
        }
        return msg;
    }
    /**
     * Create a turbo activation message
     * @return
     */
    private Message createTurboMessage()
    {
        turboInUse = turboAvailable;
        turboInUse.setStartTicks(race.getGameTicks());
        turboAvailable = null;
        return new TurboMsg();
    }

    private Message createSwitchMessage()
    {
        if (switchSent == 0 && race != null && race.isOn()) {
            CarPosition myPosition = myCar.getPosition();
            TrackPiece nextCurve = TrackCalculator.getNextCurve(race, myPosition, TrackCalculator.IGNORE_CURRENT);
            if (TrackCalculator.switchBeforeCurve(race, myPosition, nextCurve))
            {
                if (nextCurve.getAngle() < 0
                        && myPosition.getPiecePosition().getLane().getEndLaneIndex() > 0) {
                    return createSwitchMessage(LEFT);
                }
                else if (nextCurve.getAngle() > 0
                        && myPosition.getPiecePosition().getLane().getEndLaneIndex() < (race.getTrack().getLanes().size() - 1)) {
                    return createSwitchMessage(RIGHT);
                }
            }
        }
        return null;
    }


    private Message createSwitchMessage(int direction)
    {
        if (direction == LEFT)
        {
            switchSent = -1;
            switched = true;
            return new SwitchLaneMsg("Left");
        }
        if (direction == RIGHT)
        {
            switchSent = 1;
            switched = true;
            return new SwitchLaneMsg("Right");
        }
        return null;
    }


    private Message createThrottleMessage()
    {
        if (race != null && race.isOn()) {
            if (!calculator.isDragConstantInitialized() || race.getGameTicks() < 2)
            {
                return new Throttle(1.0);
            }

            CarPosition myPosition = race.locatePosition(myCar.getId());

/*            if (!calculator.isMaximumForceCurveInitialized() && !race.getPiece(myPosition.getPieceIndex()).isStraight()
                    && myPosition.getPiecePosition().getInPieceDistance() > 20)
            {
                if (turboInUse != null && turboInUse.isOn()) {
                    return new Throttle(calculator.getThrottle(9, turboInUse.getTurboFactor()));
                }
                else
                {
                    return new Throttle(calculator.getThrottle(9, 1));
                }
            }*/

            if (myPosition.getAngle() > maxAngleInPiece && currentThrottle > 0) {
                maxAngleInPiece = myPosition.getAngle();
            }
            currentThrottle = 0.0;
            // Constant velocity
            if (strategy == TEST) {
                if (privateCounter < 200) {
                    currentThrottle = 0.5;
                    maxSpeed = myCar.getCurrentSpeed();
                }
                else {
                    currentThrottle = 0;
                    if (myCar.getCurrentSpeed() < 0.2) {
                        double distance = TrackCalculator.getDistanceFromStart(race, myCar.getPosition()) - currentPosition;
                        System.out.println("Stopped from " + maxSpeed + " in " + (privateCounter - 200) + " ticks " + distance + " x");
                        privateCounter = 0;
                        currentPosition = TrackCalculator.getDistanceFromStart(race, myCar.getPosition());
                    }
                }
                privateCounter++;
            }
            else if (strategy == STRATEGY_CONSTANT) {
                currentThrottle = 0.50;
            }
            else
            {
                double distance = myCar.getCurrentSpeed();
                double targetVelocity = TrackCalculator.getMaxSpeed(race, myPosition, calculator, distance * REACTION_TIME)
                        + speedCorrection;
       //         if (privateCounter % 10 == 0) {
        //            System.out.println("Target " + targetVelocity + " for piece " + myPosition.getPieceIndex() + " cur " + myCar.getCurrentSpeed());
         //       }
                privateCounter++;
                if (Math.abs(myCar.getAngle()) > 59) {
                    targetVelocity = 0;
                }
                return createThrottleMessage(targetVelocity);
            }

            if (Math.abs(myCar.getAngle()) > MAX_ANGLE) {
                currentThrottle = 0;
            }
            if (currentThrottle > 1.0) {
                currentThrottle = 1.0;
            }

            return new Throttle(currentThrottle);
        }
        return new Throttle(0.0);
    }

    private Message createThrottleMessage(double targetVelocity)
    {
        double difference = myCar.getCurrentSpeed() - targetVelocity;
        // Going too fast
        if (difference > 0)
        {
            currentThrottle = 0.0;
        //    System.out.println("throttle constant 0");
        }
        else
        {
            if (difference < ( -1 * calculator.getMaxVelocityGainedInTime(REACTION_TIME)))
            {
                if (difference < -4 && isTurboAvailable())
                {
                    return createTurboMessage();
                }
                else {
                    currentThrottle = 1.0;
          //          System.out.println("throttle constant 1");
                }
            }
            else if (difference == Double.NaN) {
                currentThrottle = 0;
            }
            else
            {
                if (turboInUse != null && turboInUse.isOn())
                {
                    currentThrottle = calculator.getThrottle(targetVelocity, turboInUse.getTurboFactor());
  //                  System.out.println("throttle x " + currentThrottle);
                }
                else {
                    currentThrottle = calculator.getThrottle(targetVelocity, 1);
//                    System.out.println("throttle " + currentThrottle);
                }
            }
        }
        if (currentThrottle > 1.0)
            currentThrottle = 1.0;
        if (currentThrottle < 0.0)
            currentThrottle = 0.0;
        return new Throttle(currentThrottle);
    }

    // D = v^2 / (2 * friction * g)
    private double maximumSpeed(double wantedVelocity, double distance)
    {
        return Math.sqrt(2 * currentFriction * distance + Math.pow(wantedVelocity,2));
    } // FRICTION

    public void joinRace(Race currentRace)
    {
        this.race = currentRace;
        if (this.calculator == null) {
            this.calculator = new SpeedCalculator();
        }
        this.maxAngleInPiece = 0;
        if (!this.race.getRaceSession().isQualifying()) {
            watcher = new CompetitorWatcher(race, myCar);
            new Thread(watcher).start();
        }
    }

    public void leaveRace()
    {
        this.race = null;
        if (watcher != null) {
            watcher.close();
        }
        System.out.println("Constants drag " + calculator.getDragConstant()
                + " m " + calculator.getMassOfCar() + " force " + calculator.getMaximumForceCurve());
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getColor()
    {
        return color;
    }

    public Race getRace()
    {
        return race;
    }

    public void setRace(Race race)
    {
        this.race = race;
    }

    public Car getMyCar()
    {
        return myCar;
    }

    public void setMyCar(Car myCar)
    {
        this.myCar = myCar;
    }

    public void setMyCarSpec(CarSpec myCar)
    {
        this.myCar = new Car();
        this.myCar.setId(myCar);
        CarPosition position = new CarPosition();
        position.setPiecePosition(new PiecePosition());
        this.myCar.setPosition(position);
    }

    public void spawned()
    {
        this.race.setOn(true);
    }

    public void crashed()
    {
        if (myCar.getPosition().getPiecePosition().getLane().getEndLaneIndex() == initialLane) {
            calculator.setCrashSpeed(okSpeed, lastRadius);
        }
        this.race.setOn(false);
    }

    public double getCurrentFriction()
    {
        return currentFriction;
    }

    public void setCurrentFriction(double currentFriction)
    {
        this.currentFriction = currentFriction;
    }

    public double getMaxAnglePieceAngle()
    {
        return maxAnglePieceAngle;
    }

    public void setMaxAnglePieceAngle(double maxAnglePieceAngle)
    {
        this.maxAnglePieceAngle = maxAnglePieceAngle;
    }

    public int getMaxAngleRadius()
    {
        return maxAngleRadius;
    }

    public void setMaxAngleRadius(int maxAngleRadius)
    {
        this.maxAngleRadius = maxAngleRadius;
    }

    public int getStrategy()

    {
        return strategy;
    }

    public void setStrategy(int strategy)
    {
        this.strategy = strategy;
    }

    public double getFriction()
    {
        return currentFriction;
    }

    public double getMaxAngleInPiece()
    {
        return maxAngleInPiece;
    }

    public void setMaxAngleInPiece(double maxAngleInPiece)
    {
        this.maxAngleInPiece = maxAngleInPiece;
    }

    public double getCurrentThrottle()
    {
        return currentThrottle;
    }

    public void setCurrentThrottle(double currentThrottle)
    {
        this.currentThrottle = currentThrottle;
    }

    public int getSwitchSent()
    {
        return switchSent;
    }

    public void setSwitchSent(int switchSent)
    {
        this.switchSent = switchSent;
    }

    public double getLastAngle()
    {
        return lastAngle;
    }

    public void setLastAngle(double lastAngle)
    {
        this.lastAngle = lastAngle;
    }



    public void setTurboAvailable(TurboSpec turboAvailable)
    {
        this.turboAvailable = turboAvailable;
    }

    public boolean isTurboAvailable()
    {
        return this.turboAvailable != null;
    }
    public double getMaxAngle()
    {
        return maxAngle;
    }

    public void setMaxAngle(double maxAngle)
    {
        this.maxAngle = maxAngle;
    }

    public double getCurrentCurveMultiplier()
    {
        return currentCurveMultiplier;
    }

    public void setCurrentCurveMultiplier(double currentCurveMultiplier)
    {
        this.currentCurveMultiplier = currentCurveMultiplier;
    }

    public static final int STRATEGY_CONSTANT = 1;
    public static final int ADJUSTING = 0;
    public static final int STRATEGY_FASTEST = 2;
    public static final int TEST = 5;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int NO_SWITCH = 3;

    public double getOkSpeed()
    {
        return okSpeed;
    }
}
