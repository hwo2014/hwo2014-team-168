package fi.themadhat.madracer.ai;

import fi.themadhat.madracer.race.Car;
import fi.themadhat.madracer.race.CarSpec;
import fi.themadhat.madracer.race.Race;
import fi.themadhat.madracer.track.TrackCalculator;

import static java.lang.Thread.sleep;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class CompetitorWatcher implements Runnable
{

    /**
     * Muiden autojen sijainnit
     * Tulostetaan Sijoitukset kisan jälkeen
     * Moodi aika-ajo (=> nopein)
     * Moodi kisa (=> estäminen)
     * Tarkastettava ettei törmätä autoon, vain tönäistään hiukan
     * Mahdollisuuksien mukaan vaihdetaan kilpailijan eteen
     * Muiden haastaminen
     */

    private Race race;
    private Car myCar;
    private boolean close = false;

    // What to do?
    private boolean wantTurbo = false;
    private double targetLane = -1;
    private double wantSpeed = 0;
    private boolean wantBrakes = false;

    public CompetitorWatcher(Race race, Car myCar)
    {
        this.race = race;
        this.myCar = myCar;
    }

    public Car getMyCar()
    {
        return myCar;
    }

    public void setMyCar(Car myCar)
    {
        this.myCar = myCar;
    }

    public boolean isClose()
    {
        return close;
    }

    public void setClose(boolean close)
    {
        this.close = close;
    }

    public Race getRace()
    {
        return race;
    }

    public void setRace(Race race)
    {
        this.race = race;
    }

    public void close()
    {
        this.close = true;
    }

    @Override
    public void run()
    {
        while (!close) {
            try {
                double myDistance = TrackCalculator.getDistanceFromStart(race, myCar.getPosition());
                double competitorDistance;
                targetLane = -1;
                wantTurbo = false;
                wantSpeed = 0;
                wantBrakes = false;
                for (Car car: race.getCars())
                {
                    if (!car.getId().equals(myCar.getId()))
                    {
                        competitorDistance = TrackCalculator.getDistanceFromStart(race, car.getPosition());
                        if (competitorDistance < myDistance)
                        {
                            if (competitorDistance > myDistance + 50)
                            {
                                // Faster car behind us, change lanes to avoid
                                if (car.getCurrentSpeed() > myCar.getCurrentSpeed() + 1
                                        && car.getPosition().getLaneIndex() == myCar.getPosition().getLaneIndex())
                                {
                                    wantTurbo = false;
                                    if (myCar.getPosition().getLaneIndex() > 0)
                                    {
                                        targetLane = 0;
                                    }
                                    else
                                    {
                                        targetLane = 1;
                                    }
                                } // Slower or almost as fast car behind us
                                else if (car.getCurrentSpeed() <= myCar.getCurrentSpeed() + 1)
                                {
                                    targetLane = car.getPosition().getLaneIndex();
                                }
                            }
                        }
                        else
                        {
                            if (competitorDistance < myDistance + 50)
                            {
                                // Slower car ahead, turbo / change lanes
                                if (car.getCurrentSpeed() < myCar.getCurrentSpeed()
                                        && car.getPosition().getLaneIndex() == myCar.getPosition().getLaneIndex())
                                {
                                    wantTurbo = true;
                                    if (myCar.getPosition().getLaneIndex() > 0)
                                    {
                                        targetLane = 0;
                                    }
                                    else
                                    {
                                        targetLane = 1;
                                    }
                                    if (car.getCurrentSpeed() < (myCar.getCurrentSpeed() - 2))
                                    {
                                        wantBrakes = true;
                                        wantSpeed = car.getCurrentSpeed() + 2;
                                    }
                                }
                                else // Faster car ahead, or not in my lane, ignore
                                {

                                }
                            }
                        }
                    }
                }
                sleep(1000);
            }
            catch (InterruptedException e) {
            }
        }
    }

    public boolean actionIsWise()
    {
        return switchIsWise() || isTurboWise();
    }

    public boolean switchIsWise()
    {
        return (targetLane != -1 &&
            myCar.getPosition().getLaneIndex() != targetLane);
    }

    public boolean isTurboWise()
    {
        return wantTurbo;
    }
    public boolean isBrakeWise()
    {
        return wantBrakes;
    }

    public double getWiseSpeed()
    {
        return wantSpeed;
    }

    public int getSwitchDirection()
    {
        if (myCar.getPosition().getLaneIndex() == targetLane)
            return MadRacer.NO_SWITCH;
        if (myCar.getPosition().getLaneIndex() < targetLane)
        {
            return MadRacer.RIGHT;
        }
        else if (myCar.getPosition().getLaneIndex() > targetLane)
        {
            return MadRacer.LEFT;
        }
        return MadRacer.NO_SWITCH;
    }

    public void actionTaken()
    {
        targetLane = -1;
        wantTurbo = false;
        wantBrakes = false;
        wantSpeed = 0;
    }
}
