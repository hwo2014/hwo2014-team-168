package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class GameInitMsg extends Message
{
    private GameInit data;

    @Override
    public String getMsgType()
    {
        return "gameInit";
    }

    public GameInit getData()
    {
        return data;
    }
}
