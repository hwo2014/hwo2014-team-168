package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class SwitchLaneMsg extends Message
{
    private String data;

    public SwitchLaneMsg(String direction)
    {
        super();
        data = direction;
    }

    public Object getMsgData()
    {
        return data;
    }

    @Override
    public String getMsgType()
    {
        return "switchLane";
    }
}
