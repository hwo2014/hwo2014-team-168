package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.CarSpec;
import fi.themadhat.madracer.race.LapTime;
import fi.themadhat.madracer.race.RaceTime;
import fi.themadhat.madracer.race.Ranking;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class LapFinished
{

    private CarSpec car;
    private LapTime lapTime;
    private RaceTime raceTime;
    private Ranking ranking;
    private String gameId;

    public CarSpec getCar()
    {
        return car;
    }

    public void setCar(CarSpec car)
    {
        this.car = car;
    }

    public LapTime getLapTime()
    {
        return lapTime;
    }

    public void setLapTime(LapTime lapTime)
    {
        this.lapTime = lapTime;
    }

    public RaceTime getRaceTime()
    {
        return raceTime;
    }

    public void setRaceTime(RaceTime raceTime)
    {
        this.raceTime = raceTime;
    }

    public Ranking getRanking()
    {
        return ranking;
    }

    public void setRanking(Ranking ranking)
    {
        this.ranking = ranking;
    }

    public String getGameId()
    {
        return gameId;
    }

    public void setGameId(String gameId)
    {
        this.gameId = gameId;
    }
}
