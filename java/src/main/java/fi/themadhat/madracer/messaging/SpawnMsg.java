package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.CarSpec;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class SpawnMsg extends Message
{
    private CarSpec data;
    private String gameId;

    @Override
    public String getMsgType()
    {
        return "crash";
    }


    public SpawnMsg(CarSpec data)
    {
        this.data = data;
    }

    public CarSpec getData()
    {
        return data;
    }

    public Object getMsgData()
    {
        return this;
    }

}
