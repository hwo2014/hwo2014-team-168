package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.TurboSpec;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class TurboAvailableMsg extends Message
{
    private TurboSpec data;

    public TurboAvailableMsg(TurboSpec data)
    {
        this.data = data;
    }

    public TurboSpec getData()
    {
        return data;
    }

    public Object getMsgData()
    {
        return this;
    }

    @Override
    public String getMsgType()
    {
        return "turboAvailable";
    }
}
