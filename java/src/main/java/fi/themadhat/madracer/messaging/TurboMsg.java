package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class TurboMsg extends Message
{
    private String data;

    public TurboMsg()
    {
        this.data = "Vruuum vruuum!";
    }
    public TurboMsg(String data)
    {
        this.data = data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    @Override
    public String getMsgType()
    {
        return "turbo";
    }
    public String getData()
    {
        return data;
    }

    public Object getMsgData()
    {
        return data;
    }
}
