package fi.themadhat.madracer.messaging;


public class Throttle extends Message
{
    private double value;

    public Throttle(double value)
    {
        this.value = value;
    }


    @Override
    public String getMsgType()
    {
        return "throttle";
    }

    // Override parent, so that the data object contains only value
    @Override
    public Object getMsgData()
    {
        return value;
    }
}