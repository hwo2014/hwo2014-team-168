package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class MsgWrapper
{
    public final String msgType;
    public final Object data;
    public long gameTick;

    MsgWrapper(final String msgType, final Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
    MsgWrapper(final String msgType, final Object data, final long gameTick)
    {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }
    public MsgWrapper(final Message message)
    {
        this(message.getMsgType(), message.getMsgData(), message.getGameTick());
    }

    public String getMsgType()
    {
        return msgType;
    }

    public Object getMsgData()
    {
        return data;
    }

    public long getGameTick()
    {
        return gameTick;
    }
    public long setGameTick()
    {
        return gameTick;
    }
}
