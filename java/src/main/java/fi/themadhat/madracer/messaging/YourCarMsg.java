package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.CarSpec;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class YourCarMsg extends Message
{
    private CarSpec data;

    @Override
    public String getMsgType()
    {
        return "yourCar";
    }

    public CarSpec getData()
    {
        return data;
    }

    public Object getMsgData()
    {
        return data;
    }
}
