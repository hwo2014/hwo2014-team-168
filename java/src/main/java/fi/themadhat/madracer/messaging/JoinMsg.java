package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class JoinMsg extends Message
{
    private BotData data;

    public JoinMsg(BotData data)
    {
        this.data = data;
    }

    @Override
    public String getMsgType()
    {
        return "join";
    }

    public BotData getData()
    {
        return data;
    }

    public Object getMsgData()
    {
        return data;
    }
}
