package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class LapFinishedMsg extends Message
{
    private LapFinished data;

    @Override
    public String getMsgType()
    {
        return "lapFinished";
    }

    public LapFinished getData()
    {
        return data;
    }

}