package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.Race;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class GameInit extends Message
{
    public final Race race;

    public GameInit(Race race)
    {
        this.race = race;
    }

    public Race getRace()
    {
        return race;
    }

    @Override
    public String getMsgType()
    {
        return "gameInit";
    }
}
