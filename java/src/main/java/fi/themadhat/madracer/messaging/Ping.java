package fi.themadhat.madracer.messaging;


public class Ping extends Message
{
    public Ping()
    {
    }


    @Override
    public String getMsgType()
    {
        return "ping";
    }
}