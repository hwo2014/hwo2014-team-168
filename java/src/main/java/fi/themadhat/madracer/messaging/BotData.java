package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class BotData
{
    public final String name;
    public final String key;

    public BotData()
    {
        name = null;
        key = null;
    }

    public BotData(String name, String key)
    {
        this.name = name;
        this.key = key;
    }

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }
}
