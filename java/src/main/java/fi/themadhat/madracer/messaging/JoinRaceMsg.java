package fi.themadhat.madracer.messaging;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class JoinRaceMsg extends Message
{

    private BotData botId;
    private String trackName;
    private String password;
    private int carCount;

    public JoinRaceMsg(BotData data, String trackName, String password, int carCount)
    {
        this.botId = data;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    public String getMsgType()
    {
        return "joinRace";
    }

    public Object getMsgData()
    {
        return this;
    }

    public BotData getBotId()
    {
        return botId;
    }

    public void setBotId(BotData botId)
    {
        this.botId = botId;
    }

    public String getTrackName()
    {
        return trackName;
    }

    public void setTrackName(String trackName)
    {
        this.trackName = trackName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public int getCarCount()
    {
        return carCount;
    }

    public void setCarCount(int carCount)
    {
        this.carCount = carCount;
    }
}
