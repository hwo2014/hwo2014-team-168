package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.CarPosition;

import java.util.List;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class CarPositionsMsg extends Message
{
    private List<CarPosition> data;
    private String gameId;

    public CarPositionsMsg()
    {
    }

    @Override
    public String getMsgType()
    {
        return "carPositions";
    }

    public List<CarPosition> getData()
    {
        return data;
    }

}
