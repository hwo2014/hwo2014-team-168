package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.CarSpec;
import fi.themadhat.madracer.race.Disqualification;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class DnfMsg extends Message
{
    private Disqualification data;
    private String gameId;

    @Override
    public String getMsgType()
    {
        return null;
    }

    public Disqualification getData()
    {
        return data;
    }

    public void setData(Disqualification data)
    {
        this.data = data;
    }

    public String getGameId()
    {
        return gameId;
    }

    public void setGameId(String gameId)
    {
        this.gameId = gameId;
    }
}
