package fi.themadhat.madracer.messaging;

import fi.themadhat.madracer.race.CarSpec;

/**
 * @author Marjaana Sjölund <marjaana@pulmaton.fi>
 */
public class FinishMsg extends Message
{
    private CarSpec data;
    private String gameId;

    public CarSpec getData()
    {
        return data;
    }

    public void setData(CarSpec data)
    {
        this.data = data;
    }

    public String getGameId()
    {
        return gameId;
    }

    public void setGameId(String gameId)
    {
        this.gameId = gameId;
    }

    @Override
    public String getMsgType()
    {
        return "finish";
    }
}
