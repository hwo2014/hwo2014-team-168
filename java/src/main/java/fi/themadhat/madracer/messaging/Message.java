package fi.themadhat.madracer.messaging;

import com.google.gson.Gson;


public abstract class Message
{
    private long gameTick;

    public String toJson()
    {
        return new Gson().toJson(new MsgWrapper(this));
    }

    public Object getMsgData()
    {
        return this;
    }

    public abstract String getMsgType();

    public void setGameTick(long gameTick)
    {
        this.gameTick = gameTick;
    }

    public long getGameTick()
    {
        return gameTick;
    }
}


